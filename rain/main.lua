require 'src/requirements'
require 'src/Droplet'

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 1280
VIRTUAL_HEIGHT = 1280
GRAVITY = 200

function love.load()
    math.randomseed(os.time())
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        vsync = true,
        fullscreen = false,
        resizable = true
    })
    gWorld = love.physics.newWorld(0, GRAVITY)

    groundShape = love.physics.newEdgeShape(-VIRTUAL_WIDTH, 0, VIRTUAL_WIDTH * 2, 0)
    groundBody = love.physics.newBody(gWorld, 0, VIRTUAL_HEIGHT - 50, 'static')
    groundFixture = love.physics.newFixture(groundBody, groundShape)
    groundFixture:setCategory(2) -- 2 type of solid surface
    
    rain = {}
    resetBodies = {}
    groundCollisions = {}

    for i = 1,2000 do
        droplet = Droplet(
            gWorld,
            math.random(-VIRTUAL_WIDTH, VIRTUAL_WIDTH),
            math.random(-VIRTUAL_HEIGHT * 2, - 50),
            math.random(260, 280),
            math.random(0, 20),
            math.random(2.5, 3.5)
        )
        table.insert(rain, droplet)
    end

    function beginContact(a, b, col)

        if a:getUserData() and a:getUserData().Droplet then
            a:setUserData({
                Droplet=true,
                hit=true
            })
            local body = a:getBody()
            table.insert(groundCollisions, {x=body:getX(), y=body:getY()})
        end
        if b:getUserData() and b:getUserData().Droplet then
            b:setUserData({
                Droplet=true,
                hit=true
            })
            local body = b:getBody()
            table.insert(groundCollisions, {x=body:getX(), y=body:getY()})
        end
    end

    function endContact(a, b , col)
    end

    
    function preSolve(a, b, coll)
    end

    function postSolve(a, b, coll, normalImpulse, tangentImpulse)
    end
    gWorld:setCallbacks(beginContact, endContact, preSolve, postSolve)

    dropletParticles = Particle(
        0, 200, 200, 200,
        0.5, 1,
        100,
        300
    ).splash

    boxShape = love.physics.newRectangleShape(0, VIRTUAL_WIDTH / 4, VIRTUAL_WIDTH / 4, 60)
    boxBody = love.physics.newBody(gWorld, 55 + VIRTUAL_WIDTH / 2, VIRTUAL_HEIGHT / 4, 'dynamic')
    boxFixture = love.physics.newFixture(boxBody, boxShape)
    boxFixture:setCategory(2)

    roofShape = love.physics.newEdgeShape(0, 0, VIRTUAL_WIDTH/2, 0)
    roofBody = love.physics.newBody(gWorld, 50, VIRTUAL_HEIGHT - 300, 'static')
    roofFixture = love.physics.newFixture(roofBody, roofShape)
    roofFixture:setCategory(2) -- 2 type of solid surface
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end

function love.resize(w, h)
    push:resize(w, h)
end

function love.update(dt)
    gWorld:update(dt)
    dropletParticles:update(dt)
    for i, droplet in pairs(rain) do
        if droplet.fixture:getUserData().hit then
            droplet:reset(
                math.random(-VIRTUAL_WIDTH, VIRTUAL_WIDTH),
                math.random(-VIRTUAL_HEIGHT, - 30),
                math.random(260, 280),
                math.random(0, 20)
            )
            droplet.fixture:setUserData({ Droplet=true, hit=false })
        end
    end
end

function love.draw()
    push:start()
    for i, droplet in pairs(rain) do
        droplet:render()
    end
    for i, droplet in pairs(groundCollisions) do
        love.graphics.draw(dropletParticles, droplet.x, droplet.y)
        dropletParticles:emit(70)
    end
    love.graphics.setColor(255,255,255,255)
    love.graphics.print('FPS: ' .. tostring(love.timer.getFPS()))
    love.graphics.setLineWidth(4)
    love.graphics.line(groundBody:getWorldPoints(groundShape:getPoints()))
    love.graphics.polygon('fill', boxBody:getWorldPoints(boxShape:getPoints()))
    love.graphics.line(roofBody:getWorldPoints(roofShape:getPoints()))
    push:finish()
    groundCollisions = {}
end
