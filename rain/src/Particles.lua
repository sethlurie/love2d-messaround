Particle = Class{}

function Particle:init(r, g, b, lam, liftimeMin, lifetimeMax, acceleration, speed)
    -- local circle = love.graphics.newCanvas(radius, radius)
    -- love.graphics.setColor(r, g, b, lam)
    -- love.graphics.setCanvas(circle)
    -- love.graphics.circle( "fill", radius/2, radius/2, radius)
    -- love.graphics.setCanvas()
    local img = love.graphics.newImage('src/particle.png')
    self.splash = love.graphics.newParticleSystem(img, 500)
    self.splash:setParticleLifetime(liftimeMin, lifetimeMax)
    self.splash:setLinearAcceleration(-acceleration, acceleration, acceleration, -2 * acceleration)
    --self.splash:setSpeed(speed, speed * 2)
    self.splash:setColors(r, g, b, 255, r, g, b, 0) -- Fade to black.
end
