Droplet = Class{}

function Droplet:init(world, x, y, dx, dy, dropRadius)
    self.radius = dropRadius

    self.body = love.physics.newBody(world,
        x, y, "dynamic"
    )

    self.shape = love.physics.newCircleShape(dropRadius)
    self.fixture = love.physics.newFixture(self.body, self.shape)
    self.body:setLinearVelocity(dx, dy)
    self.fixture:setUserData({ 
        Droplet = true,
        hit = false
    })
    self.fixture:setCategory(1)  -- rain
    self.fixture:setMask(1) -- does not collide with rain
end

function Droplet:render()
    love.graphics.setColor(0, math.random(100, 150), math.random(200,255), math.random(180,230))
    love.graphics.circle("fill",
        self.body:getX(),
        self.body:getY(),
        self.radius
    )
end

function Droplet:reset(x, y, dx, dy)
    self.body:setPosition(x,y)
    self.body:setLinearVelocity(dx, dy)
    self.body:resetMassData()
end