Class = require 'class'
Ball = Class{}

function Ball:init(x, y, width, height)
    self.x = x
    self.y = y
    self.width = width
    self.height = height

    self.dx = 0
    self.dy = 0
end

function Ball:collides(paddle)
    local no_collide = self.x > paddle.x + paddle.width or 
        self.x + self.width < paddle.x or
            self.y > paddle.y + paddle.height or
                self.y + self.height < paddle.y
    
    return not no_collide
end

function Ball:reset()
    self.x = VIRTUAL_WIDTH / 2 - 2
    self.y = VIRTUAL_HEIGHT / 2 - 2
    self.dy = math.random(-50,50)
    self.dx = math.random(2) == 1 and 100 or -100
end

function Ball:update(dt)
    self.y = self.y + self.dy * dt
    self.x = self.x + self.dx * dt
end

function Ball:render()
    love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
end