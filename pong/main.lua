push = require 'push'
Class = require 'class'
require 'Paddle'
require 'Ball'

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 432
VIRTUAL_HEIGHT = 243

PADDLE_SPEED = 200

--[[
    Runs when the game first starts up, only once; used to initialize the game.
]]
function love.load()
    love.window.setTitle("PONG!")
    math.randomseed(os.time())
    -- use nearest-neighbor filtering on upscaling and downscaling to prevent blurring of text
    love.graphics.setDefaultFilter('nearest', 'nearest')

    -- fonts
    smallFont = love.graphics.newFont('font.ttf', 8)
    scoreFont = love.graphics.newFont('font.ttf', 32)

    -- initialize virtual resolution
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = true,
        vsync = true,
        canvas = false
    })

    player1 = Paddle(5, 30, 5, 20)
    player2 = Paddle(VIRTUAL_WIDTH - 10, VIRTUAL_HEIGHT - 50, 5, 20)

    ball = Ball(VIRTUAL_WIDTH/2 - 2, VIRTUAL_HEIGHT/2 - 2, 4, 4) 
    ball.dy = math.random(-50,50)
    ball.dx = math.random(2) == 1 and 100 or -100

    gamestate = "start"
end

function love.keypressed(key)
    if key == 'escape' then
        love.event.quit()

    elseif key == 'return' or key == 'enter' then
        if gamestate == "start" then
            gamestate = "play"
        elseif gamestate == "play" then
            gamestate = "start"
        end
    end
end

function love.resize(x, y)
    push:resize(x, y)
end

function love.draw()
    -- begin rendering at virtual resolution
    push:apply('start')
    love.graphics.clear(40, 45, 52, 255)
    love.graphics.setFont(smallFont)
    love.graphics.printf('Hello Pong!', 0, 20, VIRTUAL_WIDTH, 'center')

    -- scores
    love.graphics.setFont(scoreFont)
    love.graphics.print(tostring(player1.score), VIRTUAL_WIDTH / 2 - 50, 
        VIRTUAL_HEIGHT / 3)
    love.graphics.print(tostring(player2.score), VIRTUAL_WIDTH / 2 + 30,
        VIRTUAL_HEIGHT / 3)

    player1:render()
    player2:render()
    ball:render()

    -- end rendering at virtual resolution
    push:apply('end')
end

function love.update(dt)
    -- movement p1
    if love.keyboard.isDown('w') then
        player1.dy = -PADDLE_SPEED 
    elseif love.keyboard.isDown('s') then
        player1.dy = PADDLE_SPEED
    else
        player1.dy = 0
    end

    -- movement p2
    if love.keyboard.isDown('up') then
        player2.dy = -PADDLE_SPEED
    elseif love.keyboard.isDown('down') then
        player2.dy = PADDLE_SPEED
    else
        player2.dy = 0
    end

    if gamestate == "play" then
        -- collision detection ball and paddle
        if ball:collides(player1) then
            ball.x = player1.x + player1.width
            ball.dx = -ball.dx * 1.03
            if ball.dy < 0 then
                ball.dy = -math.random(10, 150)
            else
                ball.dy = math.random(10, 150)
            end
        end
        if ball:collides(player2) then
            ball.x = player2.x - ball.width
            ball.dx = -ball.dx * 1.03
            if ball.dy < 0 then
                ball.dy = -math.random(10, 150)
            else
                ball.dy = math.random(10, 150)
            end
        end
        -- collision y wall detection
        if ball.dy > 0 and ball.y + ball.height > VIRTUAL_HEIGHT then
            ball.y = VIRTUAL_HEIGHT - ball.height
            ball.dy = -ball.dy * 1.03
        elseif ball.y < 0 then
            ball.y = 0
            ball.dy = -ball.dy * 1.03
        end

        -- collision x wall detection
        if ball.x < 0 then
            gamestate = "start"
            ball:reset()
            player2:point()
        elseif ball.x > VIRTUAL_WIDTH then
            gamestate = "start"
            ball:reset()
            player1:point()
        end

        ball:update(dt)
    end
    player1:update(dt)
    player2:update(dt)
end