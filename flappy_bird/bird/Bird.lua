Class = require "lib/class"
Bird = Class{}

local TERMINAL_DY = 1000

function Bird:init(sprite)
    self.sprite = love.graphics.newImage(sprite)
    self.width = self.sprite:getWidth()
    self.height = self.sprite:getHeight()

    self.x = VIRTUAL_WIDTH / 3 - self.width
    self.y = VIRTUAL_HEIGHT / 2 - self.height / 2

    self.dy = 0
end

function Bird:render()
    love.graphics.draw(self.sprite, self.x, self.y)
end

function Bird:update(dt)
    self.dy = self.dy + math.min(GRAVITY * dt, TERMINAL_DY)
    self.y = self.y + self.dy * dt
end