push = require 'lib/push'
Ball = require 'bird/Bird'

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 432
VIRTUAL_HEIGHT = 243

GRAVITY = 300

local background = love.graphics.newImage("background.png")
local background_x = 0
local background_speed = 10

function love.load()
    love.window.setTitle("Flappy Bird")
    math.randomseed(os.time())
    love.graphics.setDefaultFilter('nearest', 'nearest')

    -- global fonts --
    fonts = {}
    fonts.small = love.graphics.newFont("font.ttf", 8)
    fonts.med = love.graphics.newFont("font.ttf", 16)

    -- setup virtual screen --
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = true,
        vsync = true,
        canvas = false
    })

    -- setup bird --
    player = Bird("bird/first_bird.png")
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    end
end

function love.draw()
    push:start()
    -- render background first --
    love.graphics.draw(background, -background_x, 0)

    player:render()
    push:finish()
end

function love.update(dt)
    background_x = (background_x + background_speed * dt) % 864
    if love.keyboard.isDown("space") then
        player.dy = -100
    end
    player:update(dt)
end