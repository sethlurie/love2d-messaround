push = require 'lib/push'

WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720

VIRTUAL_WIDTH = 1280
VIRTUAL_HEIGHT = 1280


function love.load()
    math.randomseed(os.time())
    push:setupScreen(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        vsync = true,
        fullscreen = false,
        resizable = true
    })
    love.keyboard.keysPressed = {}

    world = love.physics.newWorld(0,300)

    -- static ground and wall bodies
    groundBody = love.physics.newBody(world, 0, VIRTUAL_HEIGHT - 30, 'static')
    leftWallBody = love.physics.newBody(world, 0, 0, 'static')
    rightWallBody = love.physics.newBody(world, VIRTUAL_WIDTH, 0, 'static')

    -- edge shape Box2D provides, perfect for ground and walls
    edgeShape = love.physics.newEdgeShape(0, 0, VIRTUAL_WIDTH, 0)
    wallShape = love.physics.newEdgeShape(0, 0, 0, VIRTUAL_HEIGHT)

    -- affix edge shape to our body
    groundFixture = love.physics.newFixture(groundBody, edgeShape)
    leftWallFixture = love.physics.newFixture(leftWallBody, wallShape)
    rightWallFixture = love.physics.newFixture(rightWallBody, wallShape)

    balls = {}
    cirlceShape = love.physics.newCircleShape(10)
    for i = 1,1000 do
        local body = love.physics.newBody(world,
                math.random(10,1200) , math.random(10, 100), "dynamic"
            )
        body:setLinearVelocity(math.random(-60, 60),math.random(-10, 90))
        body:setGravityScale(math.random(0.01, 1))
        local fixture = love.physics.newFixture(body, cirlceShape)
        fixture:setRestitution(1.01)
        table.insert(balls, {
            body = body,
            fixture = fixture,
            r = math.random(255),
            g = math.random(255),
            b = math.random(255)
        })
    end

    -- blades --
    blades = {}
    bladeShape = love.physics.newRectangleShape(100, 10)
    numBlades = 20
    for i = 1,numBlades do
        local body = love.physics.newBody(world,
            VIRTUAL_WIDTH / (numBlades+1) * i, math.random(VIRTUAL_HEIGHT - 220, VIRTUAL_HEIGHT - 100), "kinematic"
        )
        body:setAngularVelocity(math.pi * 4)
        local fixture = love.physics.newFixture(body, bladeShape)
        table.insert(blades, {
            body = body,
            fixture = fixture
        })
    end
end

function love.update(dt)
    if love.keyboard.isDown("right") then
        for i = 1, #blades do
            b = blades[i]
            b.body:setAngularVelocity(b.body:getAngularVelocity() + 2 * math.pi * dt)
        end
    elseif love.keyboard.isDown("left") then
        for i = 1, #blades do
            b = blades[i]
            b.body:setAngularVelocity(b.body:getAngularVelocity() - 2 * math.pi * dt)
        end
    elseif love.keyboard.isDown("up") then
        for i = 1, #blades do
            b = blades[i]
            b.body:setAngularVelocity(0)
        end
    end
    world:update(dt)
    love.keyboard.keysPressed = {}
end


function push.resize(w, h)
    push:resize(w, h)
end

function love.draw()
    push:start()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setLineWidth(4)
    love.graphics.line(groundBody:getWorldPoints(edgeShape:getPoints()))
    love.graphics.line(leftWallBody:getWorldPoints(wallShape:getPoints()))
    love.graphics.line(rightWallBody:getWorldPoints(wallShape:getPoints()))

    for i = 1, #balls do
        ball = balls[i]
        love.graphics.setColor(ball.r, ball.g, ball.b, 255)
        love.graphics.circle("fill",
            ball.body:getX(),
            ball.body:getY(),
            cirlceShape:getRadius()
        )
    end

    for i = 1, #blades do
        blade = blades[i]
        love.graphics.setColor(255,255,255,175)
        love.graphics.polygon("fill",
            blade.body:getWorldPoints(bladeShape:getPoints())
        )
    end

    push:finish()
end